﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_visitor
{
    class Book : IItem
    {
        public string Name { get; private set; }
        public string Author { get; private set; }
        public double Price { get; private set; }        public Book(string name, string author, double price)
        {
            this.Name = name;
            this.Author = author;
            this.Price = price;
        }
        public override string ToString()
        {
            return "Book: " + this.Name + " by " + this.Author +
           Environment.NewLine + " -> Price: " + this.Price;
        }

        public double Accept(IVisitor visitor)
        {
            return visitor.Visit(this);
        }

    }
}
